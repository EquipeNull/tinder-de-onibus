package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/cbroglie/mustache"

	"github.com/apexskier/httpauth"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/equipenull/model"

	_ "github.com/go-sql-driver/mysql"
)

var (
	backend     httpauth.LeveldbAuthBackend
	aaa         httpauth.Authorizer
	roles       map[string]httpauth.Role
	backendfile = "auth.leveldb"

	db *gorm.DB
)

func main() {

	var err error

	db, err = gorm.Open("mysql", "TINDER_USER:H4Ma9hmbo1W6adTJ@/Tinder_Onibus")
	defer db.Close()
	if err != nil {
		log.Print("erro ao abrir o banco: " + err.Error())
	}

	os.Mkdir(backendfile, 0755)
	defer os.Remove(backendfile)

	// create the backend
	backend, err = httpauth.NewLeveldbAuthBackend(backendfile)
	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&model.Tag{})
	db.AutoMigrate(&model.Usuario{})
	db.AutoMigrate(&model.Categoria{})

	// create some default roles
	roles = make(map[string]httpauth.Role)
	roles["user"] = 30
	roles["admin"] = 80
	aaa, err = httpauth.NewAuthorizer(backend, []byte("cookie-encryption-key"), "user", roles)

	// create a default user
	username := "root"
	defaultUser := httpauth.UserData{Username: username, Role: "admin"}
	err = backend.SaveUser(defaultUser)
	if err != nil {
		panic(err)
	}

	aaa.Update(nil, nil, username, "adminadmin", "admin@localhost.com")

	usuarios := []model.Usuario{}
	db.Find(&usuarios)
	for _, usr := range usuarios {
		fmt.Println(usr)
		usr.RegisterAuth(backend, aaa)
	}

	// set up routers and route handlers
	r := mux.NewRouter()

	r.HandleFunc("/", getRoot).Methods("GET")
	r.HandleFunc("/pos", postPos)
	r.HandleFunc("/auth", getAuth)
	r.HandleFunc("/reg", getRegister)
	r.HandleFunc("/login", getLogin).Methods("GET")
	r.HandleFunc("/msg/{name}", msg)
	r.HandleFunc("/send", send)
	r.HandleFunc("/logout", logout)
	r.HandleFunc("/chat/{name}", getChat).Methods("GET")
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./assets")))

	http.Handle("/", r)
	http.ListenAndServe(fmt.Sprintf(":%d", 8000), nil)
}

func getRoot(w http.ResponseWriter, r *http.Request) {

	usr := CurrentUser(w, r)
	opt := map[string]bool{}
	if usr != nil {
		opt["logged"] = true
	}

	res, _ := mustache.RenderFile("./view/index.html", usr, opt)

	w.Write([]byte(res))
}

func getChat(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uname := vars["name"]

	usr := CurrentUser(w, r)

	var target model.Usuario
	dbErr := db.Where(&model.Usuario{Email: uname}).Find(&target)

	if usr == nil || dbErr.Error != nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	opt := map[string]model.Usuario{"Alvo": target}

	res, _ := mustache.RenderFile("./view/chat-index.html", usr, opt)

	w.Write([]byte(res))
}

func msg(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	username := vars["name"]

	var target model.Usuario
	db.Where(&model.Usuario{Email: username}).Find(&target)

	w.Header().Set("Content-type", "application/json;")
	w.Write([]byte("{\"conteudo\": \"" + target.UltimaMensagem + "\"}"))
}

func send(w http.ResponseWriter, r *http.Request) {

	type Msg struct {
		Conteudo string `json:"conteudo"`
	}

	var msg Msg

	bytes, _ := ioutil.ReadAll(r.Body)

	msg.Conteudo = string(bytes)

	user := CurrentUser(w, r)
	if user == nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	} else {
		user.UltimaMensagem = msg.Conteudo
		db.Save(user)
	}

	json.NewEncoder(w).Encode(&msg)
}

func postPos(w http.ResponseWriter, r *http.Request) {

	var pos model.Position

	json.NewDecoder(r.Body).Decode(&pos)

	json.NewEncoder(w).Encode(pos)

	usr := CurrentUser(w, r)
	if usr != nil {
		usr.Position = pos
		db.Save(usr)
	} else {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func getLogin(w http.ResponseWriter, r *http.Request) {

	if CurrentUser(w, r) != nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	bytes, err := ioutil.ReadFile("./view/login-index.html")
	if err != nil {
		log.Print(err)
	}

	w.Write(bytes)
}

func getAuth(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	usr := r.Form.Get("usuario")
	pwd := r.Form.Get("senha")

	fmt.Println(usr + ", " + pwd)

	var user model.Usuario

	db.Where(&model.Usuario{Email: usr}).First(&user)

	fmt.Println(user)

	if user.Password == pwd || true {
		aaa.Login(w, r, usr, pwd, "/")
		http.Redirect(w, r, "/", http.StatusSeeOther)
	} else {
		fmt.Println("No Ayyy")
	}
}

func getRegister(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	usr := r.Form.Get("usuario")
	pwd := r.FormValue("senha")

	fmt.Println(usr)
	fmt.Println(pwd)

	model.RegistrarUsuario(db, usr, pwd)
}

func logout(w http.ResponseWriter, r *http.Request) {
	if err := aaa.Logout(w, r); err != nil {
		fmt.Println(err)
		// this shouldn't happen
		return
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func CurrentUser(w http.ResponseWriter, r *http.Request) *model.Usuario {

	data, err := aaa.CurrentUser(w, r)
	if err == nil {

		var usr model.Usuario
		db.Where(&model.Usuario{Email: data.Username}).Find(&usr)
		return &usr
	}

	fmt.Println(err.Error())

	return nil
}
