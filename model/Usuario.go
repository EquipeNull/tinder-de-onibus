package model

import (
	"fmt"

	"github.com/apexskier/httpauth"
	"github.com/jinzhu/gorm"
)

type Usuario struct {
	Id             int64
	Email          string
	UltimaMensagem string
	Password       string
	IsAdmin        bool
	Position       Position

	Tags []Tag `gorm:"many2many:usuario_tags;"`
}

type Position struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func RegistrarUsuario(db *gorm.DB, email, pwd string) *Usuario {

	fmt.Println(email)

	usr := &Usuario{Email: email, Password: pwd}

	fmt.Println(usr)

	if db.Save(usr).Error != nil {
		return nil
	}

	return usr
}

func (self *Usuario) RegisterAuth(b httpauth.LeveldbAuthBackend, a httpauth.Authorizer) {
	b.SaveUser(httpauth.UserData{Username: self.Email, Role: "user"})

	a.Update(nil, nil, self.Email, self.Password, "NONE")
}
