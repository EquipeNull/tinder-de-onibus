package model

type Categoria struct {
	Id   int64
	Nome string
	Tags []Tag `gorm:"many2many:usuario_tag;"`
}
