package model

// Tag é uma tag que pode ser
// vinculada ao perfil de usuário,
// representando um gosto
type Tag struct {
	Id   int64
	Nome string
	Cat  Categoria
}
