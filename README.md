# Quero 1 amigo

Quero 1 amigo é um aplicativo para fazer com que você aproveite melhor seu tempo livre, permitindo a você que converse com pessoas que compartilham gostos e se encontram próximas de você.

## O Time
- Arthur Rosso
- Augusto Falcão
- Pietro Carrara
Todos alunos do ensino médio do IFRS - Canoas

## Tecnologias
Go, Bootstrap, JS, JQuery, HTML, CSS

!(https://i.imgur.com/FoSFLnq.png)

## O Resultado
Atualmente não é possível utilizar nada além do chat :(